/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.test.seleniun;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author alex
 */
public class SingIn {
    private By userNameBy = By.id("username");
    private By passwordBy = By.id("password");
    private By saveButtonBy = By.id("saveButton");
    private By errorMessageBy = By.className("ui-messages-error-summary");
    
    private WebDriver driver;
    
    public SingIn(WebDriver driver) {
        this.driver = driver;
    }
    
    public void setUserNameBy(String userName) {
        this.driver.findElement(userNameBy).sendKeys(userName);
    }
    
    public void setPasswordBy(String password) {
        this.driver.findElement(passwordBy).sendKeys(password);
    }
    
    public String getErrorMessageText() {
        return this.driver.findElement(errorMessageBy).getText();
    }
    
    public SigesaHome loginValidUser(String userName, String password) {
        this.driver.findElement(userNameBy).sendKeys(userName);
        this.driver.findElement(passwordBy).sendKeys(password);
        this.driver.findElement(saveButtonBy).click();
        
        return new SigesaHome(this.driver);
    }
    
    public SingIn loginFailsCase(String userName, String password) {
        this.driver.findElement(userNameBy).sendKeys(userName);
        this.driver.findElement(passwordBy).sendKeys(password);
        this.driver.findElement(saveButtonBy).click();
        
        return new SingIn(this.driver);
    }
}
