/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.test.seleniun;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author alex
 */
public class HelloPage {
    public WebDriver driver;
    
    private By helloBy = By.id("hello");
    private By nameBy = By.id("name");
    private By cambioBy = By.id("cambio");

        
    public HelloPage(WebDriver driver, String name) {
        this.driver = driver;
        //driver.findElement(nameBy).sendKeys(name);
    }
    
    public String getHelloMessage() {
        return driver.findElement(this.helloBy).getText();
    }
    
    public String getNameMessage() {
        return driver.findElement(this.nameBy).getText();
    }
    
    public void setNameMessage(String name) {
        driver.findElement(this.nameBy).sendKeys(name);
    }
    
    public void changeName() {
        driver.findElement(this.cambioBy).click();
    }
}
