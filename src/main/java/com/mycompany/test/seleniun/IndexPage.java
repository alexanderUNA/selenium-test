/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.test.seleniun;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author alex
 */
public class IndexPage {

    public WebDriver driver;

    private By irOtroLadoBy = By.id("ir-otro-lado");
    //private By nameBy = By.id("name");

    public IndexPage(WebDriver driver) {
        this.driver = driver;
    }

    public HelloPage navigateToHello(String name) {
        driver.findElement(irOtroLadoBy).click();
        return new HelloPage(driver, name);
    }

}
