/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.tests;

import java.time.Duration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;

/**
 *
 * @author alex
 */
public class TestLogin {

    private WebDriver driver;

    @BeforeEach
    public void setTestLogin() {
        System.setProperty(
                "webdriver.edge.driver", "src/main/java/drivers/msedgedriver");
        //this.newWebDriver();
    }

    /*public void newWebDriver() {
        EdgeOptions options = new EdgeOptions();
        //options.addArguments("--headless");

        this.driver = new EdgeDriver(options);
    }
    */

    @Test
    public void testWepBamboo() {
        //this.newWebDriver();
         EdgeOptions options = new EdgeOptions();
        //options.addArguments("--headless");

        this.driver = new EdgeDriver(options);
        this.driver.get("http://localhost:8080/WepBamboo/");

        WebElement searchBox = driver.findElement(By.id("name"));

        searchBox.sendKeys("Selenium");
        this.driver.manage().timeouts().implicitlyWait(Duration.ofMillis(1500000));
        System.out.println(searchBox.getAttribute("value"));

        Assert.assertEquals(searchBox.getAttribute("value"), "eelenium");
    }
}
