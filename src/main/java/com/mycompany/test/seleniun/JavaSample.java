/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.test.seleniun;

/**
 *
 * @author alex
 */
// Sample test in Java to run Automate session.
//import io.github.bonigarcia.wdm.WebDriverManager;
import java.time.Duration;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.junit.Assert;

public class JavaSample {

    public EdgeDriver driver;

    public void chromeSession() {
        //   WebDriverManager.chromedriver().setup();

        System.setProperty("webdriver.chrome.driver", "src/main/java/drivers/msedgedriver");
        //ChromeDriver driver = new ChromeDriver();

        //ChromeOptions options = new ChromeOptions();
        //WebDriver driver = new ChromeDriver(options);
        //driver.quit();
    }

    public void edgeSession() {
        System.setProperty("webdriver.edge.driver", "./msedgedriver");

        //EdgeOptions options = new EdgeOptions();
        driver = new EdgeDriver();

        //driver.quit();
        driver.get("https://www.google.com/");

        String title = driver.getTitle();
        driver.manage().timeouts().implicitlyWait(Duration.ofMillis(500));

        System.out.println(title);

        WebElement searchBox = driver.findElement(By.name("q"));
        WebElement searchButton = driver.findElement(By.name("btnK"));

        searchBox.sendKeys("Selenium");
        searchButton.click();

        driver.findElement(By.name("q")).getAttribute("value");

        driver.quit();
    }

    public void edgeSessionAlexPage() {
        System.setProperty("webdriver.edge.driver", "src/main/java/drivers/msedgedriver");

        EdgeOptions options = new EdgeOptions();
        // options.addArguments("--headless");
        driver = new EdgeDriver(options);

        //driver.quit();
        driver.get("http://localhost:8080/alex-page/index.html");

        driver.manage().timeouts().implicitlyWait(Duration.ofMillis(500));

        verifyTitle();

        verifyH1();

        //System.out.println(title);
        //WebElement searchBox = driver.findElement(By.id("alex-search"));
        //WebElement searchButton = driver.findElement(By.id("btn-search"));
        //searchBox.sendKeys("Selenium");
        //searchButton.click();
        //Assert.assertEquals(driver.findElement(By.id("item-was-searched")).getText(), "selenium");
        //Assert.assertEquals(driver.findElement(By.id("item-was-searched")).getText(), "Celenium");
        //driver.findElement(By.name("q")).getAttribute("value");
        //driver.quit();
    }

    private void verifyTitle() {
        String title = driver.getTitle();
        Assert.assertEquals(title, "Documenet");

        System.out.println("Title is correct!");
    }

    private void verifyH1() {
        WebElement searchBox = driver.findElement(By.id("alex-search"));
        WebElement searchButton = driver.findElement(By.id("btn-search"));
        searchBox.sendKeys("Selenium");
        searchButton.click();
        Assert.assertEquals(driver.findElement(By.id("item-was-searched")).getText(), "selenium");

        System.out.println("H1 is correct!");
    }

    public void testNavigate() {
        System.setProperty("webdriver.edge.driver", "src/main/java/drivers/msedgedriver");

        EdgeOptions options = new EdgeOptions();
        options.addArguments("--headless");
        driver = new EdgeDriver(options);

        //driver.quit();
        driver.get("http://localhost:8080/alex-page/index.html");

        IndexPage index = new IndexPage(driver);
        HelloPage hello = index.navigateToHello("Alex");
        Assert.assertEquals("Welcome", hello.getHelloMessage());
        System.out.print("Welcome message is correct");
        Assert.assertEquals("Alex", hello.getNameMessage());
        System.out.print("Name is correct");
    }
    
    public void testLogin() {
        
        EdgeOptions options = new EdgeOptions();
        //options.addArguments("--headless");
        driver = new EdgeDriver(options);

        //driver.quit();
        driver.get("http://10.0.2.134:8080/sigesa-webapp/security/login.xhtml");
        
        SingIn singIn = new SingIn(driver);
        SigesaHome sigesaHome = singIn.loginValidUser("116040110", "xxxxxxxx");
        driver.manage().timeouts().implicitlyWait(Duration.ofMillis(1500000));
        //Assert.assertEquals(sigesaHome.getProfileNameText(), "116040110 - ARCE GUTIERREZ ALEXANDER");
        Assert.assertEquals("url(\"http://10.0.2.134:8080/sigesa-webapp/javax.faces.resource/images/dashboard.png.xhtml?ln=ultima-layout\")"
                + "", sigesaHome.getImageHome());
        System.out.println("Access to Sigesa Home Page correct");
    }
    
    public void testLoginFails() {
        System.setProperty("webdriver.edge.driver", "src/main/java/drivers/msedgedriver");

        EdgeOptions options = new EdgeOptions();
        //options.addArguments("--headless");
        driver = new EdgeDriver(options);

        //driver.quit();
        driver.get("http://10.0.2.134:8080/sigesa-webapp/security/login.xhtml");
        
        SingIn singIn = new SingIn(driver);
        singIn.loginFailsCase("xxxxxxxx", "xxxxxxxx");
        driver.manage().timeouts().implicitlyWait(Duration.ofMillis(1500000));
        Assert.assertEquals("Nombre de usuario no existe", singIn.getErrorMessageText());
        System.out.println("Access to Sigesa Home fails with incorrect user");
    }
}
