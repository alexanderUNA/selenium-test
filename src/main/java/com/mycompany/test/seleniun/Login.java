/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.test.seleniun;

import java.time.Duration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;

/**
 *
 * @author alex
 */
public class Login {

    private WebDriver driver;

    @Before
    public void setUp() {
        System.setProperty(
                "webdriver.edge.driver", "src/main/java/drivers/msedgedriver");
        //this.newWebDriver();
    }

    public void newWebDriver() {
        EdgeOptions options = new EdgeOptions();
        //options.addArguments("--headless");

        this.driver = new EdgeDriver(options);
    }

    public void testLogin() {
        this.newWebDriver();

        SingIn singIn = new SingIn(driver);
        SigesaHome sigesaHome = singIn.loginValidUser("116040110", "xxxxxxxx");
        this.driver.manage().timeouts().implicitlyWait(Duration.ofMillis(1500000));
        //Assert.assertEquals(sigesaHome.getProfileNameText(), "116040110 - ARCE GUTIERREZ ALEXANDER");
        System.out.println("Access to Sigesa Home Page correct");

        this.driver.close();
    }

    public void testLoginWrongUser() {
        this.newWebDriver();

        SingIn singIn = new SingIn(driver);
        singIn.loginFailsCase("xxxxxxxx", "xxxxxxxx");
        this.driver.manage().timeouts().implicitlyWait(Duration.ofMillis(1500000));
        Assert.assertEquals("Nombre de usuario no existe", singIn.getErrorMessageText());
        System.out.println("Access to Sigesa Home fails with incorrect user");
        this.driver.close();
    }

    public void testLoginWrongPassword() {
        this.newWebDriver();

        SingIn singIn = new SingIn(driver);
        singIn.loginFailsCase("116040110", "zzzzzzzz");
        this.driver.manage().timeouts().implicitlyWait(Duration.ofMillis(1500000));
        Assert.assertEquals("Credenciales incorrectos", singIn.getErrorMessageText());
        System.out.println("Access to Sigesa Home fails with incorrect password");
        this.driver.close();
    }

    @Test
    public void testWepBamboo() {
        this.newWebDriver();
        this.driver.get("http://localhost:8080/WepBamboo/");

        WebElement searchBox = driver.findElement(By.id("name"));
        
        searchBox.sendKeys("Selenium");
         this.driver.manage().timeouts().implicitlyWait(Duration.ofMillis(1500000));
        System.out.println("gooooooo");
        
        Assert.assertEquals(searchBox.getAttribute("value"), "Selenium");
    }

}
