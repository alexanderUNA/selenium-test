/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.test.seleniun;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author alex
 */
public class SigesaHome {
    
    private By profileNameBy = By.className("profile-name");
    private By imageBackgroundBy = By.id("dashForm:board_content");
    private WebDriver driver;

    public SigesaHome(WebDriver driver) {
        this.driver = driver;
        
        if(!driver.getTitle().equals("Inicio")) {
            throw new IllegalStateException("This is not the sigea's home page, " 
                    + "the current page is: " 
                    + this.driver.getCurrentUrl());
        }
    }
    
    public String getProfileNameText() {
        this.driver.findElements(profileNameBy).forEach( (a) -> {
            System.out.println(a.getText());
            System.out.println(a.getClass());
            System.out.println(a.getTagName());
        });
        return "g";
    }
    
    public String getImageHome() {
        return this.driver.findElement(imageBackgroundBy).getCssValue("background-image");
    }
    
    
    
}
